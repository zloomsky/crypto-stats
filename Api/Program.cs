﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

using System.Net;
using System.Diagnostics;
using System;

namespace Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = CreateWebHostBuilder(args);
            Startup.ServerUrl = builder.GetSetting(WebHostDefaults.ServerUrlsKey);
            builder.Build().Run();
        }     

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) => WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
    }
}
