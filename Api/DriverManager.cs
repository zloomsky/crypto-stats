﻿using Api.Contracts;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Flurl.Http;
using Flurl;

namespace Api
{   
    public class DriverManager : IHostedService
    {
        public ConcurrentDictionary<Mode, RemoteWebDriver> Drivers { get; set; }

        public DriverManager()
        {
            Drivers = new ConcurrentDictionary<Mode, RemoteWebDriver>();
        }
       
        private RemoteWebDriver InitDriver()
        {
            var options = new ChromeOptions();
            options.AddArguments("--window-size=900,700", "headless");
            options.AddExcludedArgument("enable-automation");
            options.AddAdditionalCapability("useAutomationExtension", false);
            var service = ChromeDriverService.CreateDefaultService(Path.GetDirectoryName(Assembly.GetAssembly(GetType()).Location), "chromedriver.exe");
            try
            {
                var driver = new ChromeDriver(service, options);
                Trace.TraceInformation($"Driver session created.");
                return driver;
            }
            catch (Exception e)
            {
                Trace.TraceError($"Driver exeption ${e.ToString()}");
                throw e;
            }
        }

        private async Task<bool> LoadPageWithMode(Mode mode)
        {
            if (!Drivers.ContainsKey(mode))
                return false;

            var driver = Drivers[mode];
            await Task.Run(() => driver.Navigate().GoToUrl($"{Startup.ServerUrl}/#{mode}"));

            return true;
        }

        public async Task TriggerLoadPage()
        {
            var code = HttpStatusCode.BadRequest;
            while (code != HttpStatusCode.OK)
            {
                try
                {
                    var result = await Startup.ServerUrl.AppendPathSegment("api/stats/init").PostStringAsync("");
                    code = result.StatusCode;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation($"Init fail {ex.Message}");
                }

                await Task.Delay(1000);
            }
            Trace.TraceInformation($"Init done.");
        }
               
        public async Task<bool> LoadPage()
        {
            var results = await Task.WhenAll(LoadPageWithMode(Mode.White), LoadPageWithMode(Mode.Dark));
            return results.All(x => x);
        }

        public void Stop()
        {
            foreach (var driver in Drivers.Values)
            {
                Trace.TraceInformation($"Start disposing driver {driver.SessionId}.");
                try
                {
                    driver.Quit();
                }
                catch (Exception e)
                {
                    Trace.TraceError($"Driver {driver.SessionId} dispose exeption {e.ToString()}");
                    throw;
                }
                Trace.TraceInformation($"Driver {driver.SessionId} disposed.");
            }
            Drivers.Clear();
        }
               
        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.WhenAll(
                Task.Run(() => Drivers.TryAdd(Mode.Dark, InitDriver()), cancellationToken), 
                Task.Run(() => Drivers.TryAdd(Mode.White, InitDriver()), cancellationToken));
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.Run(Stop, cancellationToken);
        }
    }   
}
