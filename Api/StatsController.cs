﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Flurl.Http;
using Api.Contracts;
using System.Linq;
using System.Collections.Generic;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatsController : ControllerBase
    {
        const string coinsUrl = "https://ct-pricing-api-1619w-au-dev.azurewebsites.net/api/Prices/aud/change/1d";
        const string coinNamesUrl = "https://trade.cointree.com/api/exchangeconfig/supportedcoins";
        const string portfolioUrl = "https://trade.cointree.com/api/portfolio/best";
        const string iconUrl = "https://raw.githubusercontent.com/atomiclabs/cryptocurrency-icons/master/svg";
        private readonly DriverManager _driverManager;

        public StatsController(DriverManager driverManager)
        {
            _driverManager = driverManager;
        }

        private async Task LoadIconAsync(CoinData data)
        {
            try
            {
                data.Icon = await $"{iconUrl}/color/{data.Info.Symbol.ToLower()}.svg".GetStringAsync();
            }
            catch (Exception)
            {
                data.Icon = await $"{iconUrl}/color/generic.svg".GetStringAsync();
            }
          
        }

       
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var coins = await coinsUrl.GetJsonAsync<Coin[]>();
            var names = await coinNamesUrl.GetJsonAsync<CoinInfo[]>();
            var portfolios = await portfolioUrl.GetJsonAsync<CoinPortfolio[]>();

            var coinData = coins.Join(names, i => i.Symbol, o => o.Symbol, (i, o) => new CoinData { Coin = i, Info = o });

            var popular = coinData
                .OrderByDescending(x => x.Coin.Spot)
                .Take(5)
                .ToArray();

            var best = portfolios
                .OrderByDescending(x => x.RelativePerformanceIndex)
                .Where(x => x.RelativePerformanceIndex > 0)
                .Take(2);

            var fast = coinData
                .OrderByDescending(x => x.Coin.ChangePercent)
                .Take(best.Count() < 2 ? 5 : 3)
                .ToArray();

            await Task.WhenAll(popular.Concat(fast).Select(LoadIconAsync));
            
            return Ok(new { popular, best, fast });
        }

        [HttpPost("init")]
        public async Task<IActionResult> LoadModes()
        {
            var loaded = await _driverManager.LoadPage();
            if (!loaded)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpGet("image")]
        public IActionResult GetImageAsync(Mode mode = Mode.White)
        {         
            return File(_driverManager.Drivers[mode].GetScreenshot().AsByteArray, "image/jpeg");
        }
    }
}
