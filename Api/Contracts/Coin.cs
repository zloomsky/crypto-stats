﻿namespace Api.Contracts
{
    public class Coin
    {
        public float ChangePercent { get; set; }
        public float CurrentRate { get; set; }
        public float Spot { get; set; }
        public string Symbol { get; set; }
    }
}
