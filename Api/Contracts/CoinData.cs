﻿namespace Api.Contracts
{
    public class CoinData
    {
        public Coin Coin { get; set; }
        public CoinInfo Info { get; set; }
        public string Icon { get; set; }
    }
}
