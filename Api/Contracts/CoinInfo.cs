﻿namespace Api.Contracts
{
    public class CoinInfo
    {
        public string CoinName { get; set; }
        public string Symbol { get; set; }
    }
}
