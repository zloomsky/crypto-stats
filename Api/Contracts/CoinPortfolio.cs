﻿namespace Api.Contracts
{
    public class CoinPortfolio
    {
        public CoinShare[] CoinShare { get; set; }
        public decimal RelativePerformanceIndex { get; set; }
    }
}
