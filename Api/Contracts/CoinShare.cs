﻿namespace Api.Contracts
{
    public class CoinShare
    {
        public string CoinName { get; set; }
        public float Share { get; set; }
    }
}
