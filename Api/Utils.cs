﻿using System;
using System.IO;
using System.Linq;

namespace Api
{
    public static class Utils
    {
        public static string GetResource(string name)
        {
            var assembly = typeof(Program).Assembly;
            var resource = assembly.GetManifestResourceNames().FirstOrDefault(r => r.EndsWith(name));
            if (string.IsNullOrEmpty(resource))
            {
                throw new Exception("Resource not found.");
            }

            using (var stream = assembly.GetManifestResourceStream(resource))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
