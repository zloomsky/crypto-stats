import React, { CSSProperties } from "react";
import { render } from "react-dom";
import Coin from "./coin";
import "./index.css";
import "./logo.png";
import { Data } from "./models";
import Portfolio from "./portfolio";
import { get } from "./utils";

const styles: { [s: string]: CSSProperties; } = {
  root: { display: "flex", justifyContent: "center", flexDirection: "column", alignItems: "center", height: "100vh" },
  stats: { display: "flex", justifyContent: "center" },
  popCoins: { display: "flex", flexDirection: "column" },
  fastCoins: { display: "flex", flexDirection: "column" },
  caption: { fontSize: "16.5px", fontWeight: 400, fontStyle: "normal", padding: "10px 0" },
  best: { display: "flex" },
};

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December",
];

class StatsComponent extends React.Component<{}, Data> {

  private interval: NodeJS.Timeout;

  constructor(props: {}) {
    super(props);
    this.state = null;
    this.interval = setInterval(async () => this.setState(await get<Data>("api/stats")), 15000);
  }

  public async componentDidMount() {
    this.setState(await get<Data>("api/stats"));
  }

  public render() {
    if (!this.state) { return null; }
    return (
      <div id="stats" className={window.location.hash.replace("#", "").toLowerCase()} style={styles.root}>
        <img alt="" src="images/logo.png"></img>
        <div style={styles.stats}>
          <div style={styles.popCoins}>
            <div style={styles.caption}>Popular coins</div>
            {this.state.popular.map((x, i) => <Coin key={`pop_${i}`} data={x} />)}
          </div>
          <div style={{ width: 50 }}></div>
          <div style={styles.fastCoins}>
            <div style={styles.caption}>Fast movers</div>
            {this.state.fast.map((x, i) => <Coin key={`fas_${i}`} data={x} />)}
            <div style={styles.caption}>Best performing portfolios</div>
            <div style={styles.best}>
              {this.state.best.map((x, i) => <Portfolio key={`bes_${i}`} data={x} />)}
            </div>
          </div>
        </div>
        <div style={styles.caption}>
          {`${monthNames[new Date().getMonth()]} ${new Date().getDate()}, ${new Date().getFullYear()}`}
        </div>
      </div>
    );
  }
}

render(<StatsComponent />, document.getElementById("root"));
