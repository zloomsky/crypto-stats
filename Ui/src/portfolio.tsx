import React, { CSSProperties } from "react";
import { ChartData, Doughnut } from "react-chartjs-2";
import { CoinPortfolio, CoinShare } from "./models";

const styles: { [s: string]: CSSProperties; } = {
  root: { display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "space-around" },
  chart: { display: "flex", flex: "auto" },
  legend: { display: "flex", flexDirection: "column", fontSize: "11px", padding: "0 10px" },
  legendItem: { display: "flex", alignItems: "center", justifyContent: "space-between" },
  legendItemLeft: { display: "flex", alignItems: "center" },
  legendDot: { borderRadius: "50%", width: "12px", height: "12px" },
  legendText: { padding: "0 5px" },
  perfText: { fontSize: "11px", padding: "5px" },
};

const colors = ["#639b22", "#51667e", "#9b6539", "#d24449", "#353937"];
const defaultColor = "#e5e5e5";

export default (props: { data: CoinPortfolio }) => {
  const { coinShare, relativePerformanceIndex } = props.data;

  const chartData: ChartData<Chart.ChartData> = {
    datasets: [{ data: coinShare.map(x => x.share), backgroundColor: colors }],
  };

  const options: Chart.ChartOptions = {
    maintainAspectRatio: false,
    responsive: false,
    cutoutPercentage: 30,
    legend: {
      display: false,
      position: "right",
      labels: {
        usePointStyle: true,
      },
    },
  };

  const renderLegendItem = (share: CoinShare, index: number) => {
    const color = index >= colors.length ? defaultColor : colors[index];
    return (
      <div key={index} style={styles.legendItem}>
        <div style={styles.legendItemLeft}>
          <div style={{ ...styles.legendDot, backgroundColor: color }}></div>
          <div style={styles.legendText}>{share.coinName}</div>
        </div>
        <div style={styles.legendText}>{share.share.toFixed(2)}%</div>
      </div>
    );
  };

  return (
    <div style={styles.root}>
      <div style={styles.chart}>
        <div>
          <Doughnut width={70} height={70} data={chartData} options={options} />
        </div>
        <div style={styles.legend}>
          {coinShare.map(renderLegendItem)}
        </div>
      </div>
      <div style={styles.perfText}>Performance - {relativePerformanceIndex.toFixed(2)}%</div>
    </div>
  );
};
