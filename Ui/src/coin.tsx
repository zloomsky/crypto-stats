import React, { CSSProperties } from "react";
import { CoinData } from "./models";

const size = 21;
const styles: { [s: string]: CSSProperties; } = {
  root: {
    display: "flex",
    borderTop: "1px solid #8080801c",
    padding: "15px 0px",
    justifyContent: "space-between",
    alignItems: "center",
  },
  group: { display: "flex" },
  icon: { width: size, height: size },
  text: { fontSize: "14px", padding: "0 5px" },
  nameText: { fontSize: "14px", fontWeight: 700, fontStyle: "normal", padding: "0 5px" },
};

const getSvgContent = (content: string) => {
  const div = document.createElement("div");
  div.innerHTML = content.trim();
  return div.firstElementChild.innerHTML;
};

export default (props: { data: CoinData }) => {
  const { coin, icon, info } = props.data;
  const svgContent = getSvgContent(icon);
  const [sign, color] = coin.changePercent === 0 ? [" ", ""] : coin.changePercent > 0 ? ["+", "#8bc34a"] : ["-", "#d24449"];

  return (
    <div style={styles.root}>
      <div style={styles.group}>
        <div style={styles.icon}>
          <svg width={size} height={size}
            viewBox="0 0 32 32"
            dangerouslySetInnerHTML={{ __html: svgContent }}>
          </svg>
        </div>
        <div style={styles.nameText}>{info.coinName} ({info.symbol})</div>
      </div>
      <div style={{ width: 50 }}></div>
      <div style={styles.group}>
        <div style={styles.text}>${coin.currentRate.toFixed(2)}</div>
        <div style={{ ...styles.text, color }}>{sign}{Math.abs(coin.changePercent).toFixed(2)}%</div>
      </div>
    </div>
  );
};
