export interface Coin {
  changePercent: number;
  currentRate: number;
  spot: number;
  symbol: string;
}

export interface CoinInfo {
  coinName: string;
  symbol: string;
}

export interface CoinShare {
  coinName: string;
  share: number;
}

export interface CoinPortfolio {
  coinShare: CoinShare[];
  relativePerformanceIndex: number;
}

export interface CoinData {
  coin: Coin;
  info: CoinInfo;
  icon: string;
}

export interface Data {
  popular: CoinData[];
  fast: CoinData[];
  best: CoinPortfolio[];
}
