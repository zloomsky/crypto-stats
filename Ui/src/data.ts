import { useEffect, useState } from "react";

const getData = <T>(url: string, defaultState: T) => {
  const [data, setData] = useState<T>(defaultState);

  const request = async () => {
    const response = await fetch(url);
    const json = await response.json();
    setData(json);
  };

  useEffect(() => { request(); }, []);
  return [data];
};

export { getData };
